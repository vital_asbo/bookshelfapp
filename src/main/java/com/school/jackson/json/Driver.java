package com.school.jackson.json;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;

public class Driver {

    public static void main(String[] args) {

        ObjectMapper mapper = new ObjectMapper();

        try {

            Student student =
                    mapper.readValue(new File("data/sample-full.json"), Student.class);

            System.out.println("First name: " + student.getFirstName2());
            System.out.println("Second name: " + student.getSecondName());
            System.out.println("Lang: " + Arrays.toString(student.getLanguages()));
            System.out.println("Address: " + student.getAddress());


        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
